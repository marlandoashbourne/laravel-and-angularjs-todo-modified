<!doctype html>
<html lang="en" ng-app>
<head>
    <meta charset="UTF-8">
    <title>Angular Training</title>
    <style>
        small { font-size: .8em; color: grey; }
    </style>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
</head>

<body ng-controller="TodosController">
    <div class="container">   
        <h1>
            Todos
            <small ng-if="remaining()">({{ remaining() }} remaining)</small>
        </h1>
        <div class="row">
            <div class="col-md-4">
                 <input type="text" placeholder="Filter todos" class="form-control" ng-model="search">
            </div>
        </div>

<hr>
    <div class="row">
        <div class="col-md-4">
            <table class="table table-striped">
                <tr ng-repeat='todo in todos | filter:search' >
                    <td><input type="checkbox" ng-model="todo.completed"  ng-change="updateTodo(todo)"  ng-style="myStyle"></td>
                    <td ng-hide="todo.completed">{{todo.body }} </td>
                    <td ng-hide="todo.completed"><button class="btn btn-danger btn-xs" ng-click="deleteTodo($index)">  <span class="glyphicon glyphicon-trash" ></span></button></td>
                    <td style="background-color:#ECECEC" ng-show="todo.completed">{{todo.body }} </td>
                    <td style="background-color:#ECECEC" ng-show="todo.completed"><button class="btn btn-danger btn-xs" ng-click="deleteTodo($index)">  <span class="glyphicon glyphicon-trash" ></span></button></td>
               
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <table class="table table-striped">
                <tr>
<!--                     <td><p class="danger">Delete All Selected</p></td>
 -->                    <td><button class="btn btn-danger btn-xs" ng-click="deleteSelected()">  Delete All Selected</button></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="form-group has-success">

        <form ng-submit="addTodo()">
            <div class="row">
                <div class="col-md-3">
                     <input type="text" placeholder="Add new task" class="form-control"ng-model="newTodoText">
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-primary">Add Task</button>
                </div>
            </div>
        </form>
    </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.5/angular.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
</body>
</html>
