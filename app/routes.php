<?php

Route::get('/', function()
{
    return View::make('index');
});

Route::get('todos', function()
{
    return Todo::all();
});

Route::post('todos', function()
{
    return Todo::create(Input::all());
});

Route::any('todo-update', function()
{

    $input = Input::all();

    $updateInput = [
    	'completed' => $input['completed']
    ];

	DB::table('todos')->where('id','=', $input['id'])->update($updateInput);

});

Route::any('todo-delete', function()
{

	 DB::table('todos')->where('id', '=', Input::all() )->delete();
});

Route::any('todo-delete-selected', function()
{

	 DB::table('todos')->where('completed', '=', true )->delete();
});


