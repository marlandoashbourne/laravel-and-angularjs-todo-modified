function TodosController($scope, $http) {

    $http.get('/todos').success(function(todos) {
        $scope.todos = todos;
    });

    $scope.remaining = function() {
        var count = 0;

        angular.forEach($scope.todos, function(todo) {
            count += todo.completed ? 0 : 1;
        });

        return count;
    }

    $scope.addTodo = function()
     {
       console.log($scope.newTodoText);

       if($scope.newTodoText)
       {

        var todo = {
            body: $scope.newTodoText,
            completed: false
        };

        $scope.todos.push(todo);

        $http.post('todos', todo);
       }
       else
       {
        alert("Todo Cant Be Empty!");
       }

    };

     $scope.deleteTodo = function(index)
    {
        console.log(index);

            // $http.post('todo-delete', index);

        var todo = $scope.todos[index];
 
        $http.post('todo-delete', todo.id)
            .success(function() {
                $scope.todos.splice(index, 1);
 
            });


    }
    
   $scope.updateTodo = function(todo) 
   {
   
      $http.put('/todo-update', todo)
      .success(function() {
        
     
      });
    }

    $scope.deleteSelected = function()
    {
 
      $http.put('/todo-delete-selected')
      .success(function() {

         $http.get('/todos').success(function(todos) {
                $scope.todos = todos;
            });
      });  
    } 
}
